package problems;

/** 
 * @see http://projecteuler.net/problem=7
 *
 * @author Daniel Maslo <http://www.danielmaslo.com>
 */
public class E007 {

	public static int solution() {
		int input = 10001;
		int counter = 0;
		int num = 0;
		
		while (counter < input) {
			num++;
			
			if (E007.isPrime(num)) {
				counter++;
			}
		}
		
		return(num);
	}
	
	public static boolean isPrime(int x) {
		if (x < 2) {
			return(false);
		}
		else if (x < 4) {
			return(true);
		}
		else {
			for (int i = 2; i < (x/2) + 1; i++) {
				if (x % i == 0) {
					return(false);
				}
			}
		}
		
		return(true);
	}
	
}
