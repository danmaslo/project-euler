package problems;

import java.util.ArrayList;

/** 
 * @see http://projecteuler.net/problem=18
 *
 * @author Daniel Maslo <http://www.danielmaslo.com>
 */
public class E018 {

    public static long solution() {
        
        int[][] input = { 
                        {3, 4, 6, 3},
                        {7, 4, 9, 0},
                        {2, 5, 0, 0},
                        {8, 0, 0, 0},
                        };
        
        int dim = 4;    
        long result = 0;
        
     
        Item[][] items = new Item[4][4];
        
        for (int i = dim; i >= 0; i--) {
            
            for (int j = 0; j < i; j++) {                
                    System.out.println(j + " " + (i - j - 1));                
            }   
            
            System.out.println("");
        }
        
        
        return(result);
    }
	
}

class Item {
    public int value;
    public int sum;
    public Item i;
    public Item j;
        
    public Item(Item i_, Item j_, int value_) {
        this.i = i_;
        this.j = j_;
        this.value = value_;
        if (i_ != null) {
            this.sum = i_.sum + i_.value + j_.sum + j_.value;
        }
        else {
            this.sum = 0;
        }
    }   
        
}