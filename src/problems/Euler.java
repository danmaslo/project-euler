package problems;

/**
 * 
 * Main class, running the solution class here.
 * 
 * @see http://projecteuler.net
 * 
 * @author Daniel Maslo <http://www.danielmaslo.com>
 */
public class Euler {
	
	public static void main(String... args) {
		System.out.println(E018.solution());
	}
	
}
